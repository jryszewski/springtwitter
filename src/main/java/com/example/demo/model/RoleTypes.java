package com.example.demo.model;

public enum RoleTypes {

    ROLE_ADMIN,
    ROLE_USER;
}
