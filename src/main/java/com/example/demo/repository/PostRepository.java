package com.example.demo.repository;


import com.example.demo.model.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    //@Query("select p from Post p where p.user.id = :userId")
    List<Post> findAllByUserId(@Param("userId") Long userId);


}
