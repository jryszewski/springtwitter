package com.example.demo.controller;


import com.example.demo.model.dto.UserDto;
import com.example.demo.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @GetMapping("/register")
    public ModelAndView userView1() {
        return new ModelAndView("register", "userToInsert", new UserDto());
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute UserDto userDto) {
        registerService.registerUser(userDto);
        return "redirect:/index";
    }
}
