package com.example.demo.controller;


import com.example.demo.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HomeController {

    @Autowired
    private PostService postService;

    @GetMapping("/home")
    public ModelAndView getAllNewPosts() {
        return new ModelAndView("home", "newList", postService.getAllNewPosts());
    }
}
