package com.example.demo.controller;

import com.example.demo.model.dto.PostDto;
import com.example.demo.model.entity.Post;
import com.example.demo.model.entity.User;
import com.example.demo.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping("/home/addpost")
    public ModelAndView postView() {
        return new ModelAndView("addpost", "postToInsert", new PostDto());
    }

    @PostMapping("/home/addpost")
    public String addPost(@ModelAttribute PostDto postsDto) {
        postService.addPost(postsDto);
        return "redirect:/home";
    }
//    ///////
//    @PutMapping("/upVote")
//    public void voteUp(@ModelAttribute PostDto postDto) {
//         postService.addPost(postDto);
//    }

    @GetMapping("/home/post")
    public ModelAndView getAllPosts() {
        return new ModelAndView("post", "postList", postService.getAllPosts());
    }


    @GetMapping("/home/postById")
    public ModelAndView getPostsById(User user) {
        return new ModelAndView("post", "postList", postService.getPostsByUserID(user.getId()));
    }

    @PostMapping("/deletepost")
    public String deletePost(@ModelAttribute("post") PostDto postDto) {
        postService.deletePost(postDto.getId());
        return "redirect:/home/post";
    }
}



