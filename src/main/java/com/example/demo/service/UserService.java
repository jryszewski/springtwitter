package com.example.demo.service;


import com.example.demo.model.dto.UserDto;
import com.example.demo.model.entity.User;
import com.example.demo.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private ModelMapper mapper;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public UserService(ModelMapper mapper, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.mapper = mapper;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void addUser(UserDto userDto) {
        User user = mapper.map(userDto, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user = userRepository.save(user);
        System.out.println(user);


        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("User: name=" + authentication.getName() + " roles=" + authentication.getAuthorities());
    }

    public List<UserDto> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(u -> mapper.map(u, UserDto.class))
                .collect(Collectors.toList());
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

//    // edytowanie usera po imieniu
//    public User updateUserByName(String name, User newUser) {
//        return userRepository.findUserByName(name)
//                .map(user -> {
//                    user.setName(newUser.getName());
//                    user.setSurname(newUser.getSurname());
//                    user.setGender(newUser.getGender());
//                    user.setAge(newUser.getAge());
////                    user.setLogin(newUser.getLogin());
//                    user.setEmail(newUser.getEmail());
////                    user.setPassword(newUser.getPassword());
//                    return userRepository.save(user);
//
//                }).orElseThrow(() -> new UserNotFoundException("Brak użytkownika o imieniu: " + name));
//    }

//    public User editUser(User user) {
//        return userRepository.save(user);
//    }
}
