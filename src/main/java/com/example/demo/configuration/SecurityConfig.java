package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                    .authorizeRequests()
                    .antMatchers("/").hasAnyAuthority("ROLE_ADMIN", "ROLE_USER")
                    .antMatchers("/adduser/**").hasAnyAuthority("ROLE_ADMIN")
                    .antMatchers("/users/**").hasAnyAuthority("ROLE_ADMIN")
                    .antMatchers("/home/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_USER")
                    .antMatchers("/post/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_USER")

                    .antMatchers("/addpost/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_USER")
                    .anyRequest().permitAll()
                .and()
                    .csrf().disable()
                    .headers().frameOptions().disable()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .usernameParameter("login")
                    .passwordParameter("password")
                    .loginProcessingUrl("/login-process")
                    .failureUrl("/login-error")
                    .defaultSuccessUrl("/home")
                .and()
                    .logout()
                    .logoutSuccessUrl("/index.html"); // /login
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user")
                .password(passwordEncoder.encode("password"))
                .roles("USER")
                .and()
                .withUser("superuser")
                .password(passwordEncoder.encode("password"))
                .roles("ADMIN");
        auth.jdbcAuthentication()
                .usersByUsernameQuery("select u.login, u.password, 1 from user u where u.login =?")
                .authoritiesByUsernameQuery("select u.login, u.role, 1, from user u where u.login=?")
                .dataSource(jdbcTemplate.getDataSource())
                .passwordEncoder(passwordEncoder);
    }
}
